-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 30 Janvier 2018 à 18:42
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `animalworld`
--

-- --------------------------------------------------------

--
-- Structure de la table `aw_articles`
--

CREATE TABLE `aw_articles` (
  `art_id` int(200) NOT NULL COMMENT 'identifiant de l''article',
  `art_nom` varchar(90) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nom de l''article',
  `art_prix` int(90) NOT NULL COMMENT 'prix de l''article',
  `art_image` varchar(99) COLLATE utf8_unicode_ci DEFAULT NULL,
  `art_qtestock` int(90) NOT NULL COMMENT 'nombre d''exemplaires de l''article en stock'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_articles`
--

INSERT INTO `aw_articles` (`art_id`, `art_nom`, `art_prix`, `art_image`, `art_qtestock`) VALUES
(3, 'Os', 10, 'images_articles/os.jpg', -5),
(4, 'Collier', 10, 'images_articles/collier.jpg', 25),
(5, 'Perchoir', 10, 'images_articles/perchoir.jpg', 16),
(6, 'Foin', 7, 'images_articles/foin.jpg', 21),
(7, 'Paquet de graines', 8, 'images_articles/paquet_graines_perroquet.jpg', 17),
(8, 'Boite de criquets', 10, 'images_articles/boite_criquets.jpg', 7),
(9, 'Cage', 12, 'images_articles/cage.jpg', 16),
(10, 'Laisse', 7, 'images_articles/laisse.jpg', 12),
(11, 'Balle pour chien', 9, 'images_articles/balle-tennis-chiens.jpg', 15),
(12, 'Souris en peluche', 9, 'images_articles/souris_peluche.jpg', 20);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `aw_articles`
--
ALTER TABLE `aw_articles`
  ADD PRIMARY KEY (`art_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `aw_articles`
--
ALTER TABLE `aw_articles`
  MODIFY `art_id` int(200) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de l''article', AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
