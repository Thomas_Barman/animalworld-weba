<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="animalerie.css">
	<title>Créer un compte</title>


</head>
<body>
	<style type="text/css">
		#FormTable {
			margin: auto;
		}
		#DataTable tr {
			height : 30px;
		}
		#DataTable td#envoyer {
			text-align: center;
			height : 40px;
		}
		input
		{
			float: right;
		}

	</style>
	

	<div id="millieu">
        <table cellpadding="0" cellspacing="0" id="FormTable">
			<tr>
				<td>
					<?php

					$NomForm = "";
					$PrenomForm = "";
					$LoginForm = "";
					$MdpForm = "";
					$AdresseForm = "";
					$AnneeNaissanceForm = "";
					$EmailForm = "";

					if (isset($_GET['nom']))
					{
						$NomForm = $_GET['nom'];
						$PrenomForm = $_GET['prenom'];
						$LoginForm = $_GET['login'];
						$MdpForm = $_GET['mdp'];
						$AdresseForm = $_GET['adresse'];
						$AnneeNaissanceForm = $_GET['anneeNaissance'];
						$EmailForm = $_GET['email'];

						echo '<h2>Ce login existe déjà. Veuillez en choisir un autre.</h2>';
					}

					?>

					<form action="traitementcreationclient.php" method="post">
						<fieldset id="form_client">
							<legend>Entrez vos informations personnelles</legend>
							<table cellpadding="0" cellspacing="0" id="DataTable">
							<tr>
								<td><span id="lblNom">Nom :</span></td>
								<td>
									<input name="txtNom" type="text" id="txtNom" class="txtBox" required value=<?php echo "'".$NomForm."'"?>/>&nbsp;
								</td>
							</tr>
							<tr>
								<td><span id="lblPrenom">Pr&eacute;nom :</span></td>
								<td>
									<input name="txtPrenom" type="text" id="txtPrenom" class="txtBox"  required value=<?php echo "'".$PrenomForm."'"?>/>&nbsp;
								</td>
							</tr>
              <tr>
								<td><span id="lblLogin">login :</span></td>
								<td>
									<input name="txtLogin" type="text" id="txtLogin" class="txtBox"  required value=<?php echo "'".$LoginForm."'"?> />&nbsp;
								</td>
							</tr>
              <tr>
								<td><span id="lblMdp">Mot de passe :</span></td>
								<td>
									<input name="txtMdp" type="password" id="txtMdp" class="mdpBox"  required value=<?php echo "'".$MdpForm."'"?> />&nbsp;
								</td>
							</tr>
							<tr>
								<td><span id="lblAdresse">Adresse :</span></td>
								<td>
									<input name="txtAdresse" type="text" id="txtAdresse" class="txtBox"  required value=<?php echo "'".$AdresseForm."'"?> />&nbsp;
								</td>
							</tr>
							<tr>
								<td><span id="lblAnneeNaissance">Année de naissance :</span></td>
                                <td>
									<input name="txtAnneeNaissance" type="text" id="txtAnneeNaissance" class="txtBox"  required value=<?php echo "'".$AnneeNaissanceForm."'"?> />&nbsp;
								</td>

							</tr>
              <tr>
								<td><span id="lblEmail">Adresse email :</span></td>
                                <td>
									<input name="txtEmail" type="text" id="txtEmail" class="txtBox"  required value=<?php echo "'".$EmailForm."'"?> />&nbsp;
								</td>

							</tr>
							<tr>
								<td colspan=3 id="envoyer"><input type="submit" name="btEnvoyer" value="Valider" class="btn"/></td>
							</tr>
						</table>
						</fieldset>
					</form>
				</td>
			</tr>
		</table>
	</div>


</body>
</html>
