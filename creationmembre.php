<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="mep.css">
	<title>Créer un compte</title>


</head>
<body>
	<style type="text/css">
		#FormTable {
			margin: auto;
		}
		#DataTable tr {
			height : 30px;
		}
		#DataTable td#envoyer {
			text-align: center;
			height : 40px;
		}
		input
		{
			float: right;
		}

	</style>
	
	<div id="rectangle">
			<div id="voicinosarticles">
				<div>Bienvenue chez Animalworld !</div>
				
				
			</div>
	</div>
	<div id="millieu">
        <table cellpadding="0" cellspacing="0" id="FormTable">
			<tr>
				<td>
					<?php

					$NomForm = "";
					$PrenomForm = "";
					$LoginForm = "";
					$MdpForm = "";
					$AdresseForm = "";
					$AnneeNaissanceForm = "";
					$EmailForm = "";

					if (isset($_GET['nom']))
					{
						$NomForm = $_GET['nom'];
						$PrenomForm = $_GET['prenom'];
						$LoginForm = $_GET['login'];
						$MdpForm = $_GET['mdp'];
						$AdresseForm = $_GET['adresse'];
						$AnneeNaissanceForm = $_GET['anneeNaissance'];
						$EmailForm = $_GET['email'];
					}

					?>

					<form  class="formulaire" action="traitementcreationmembre.php" method="post">
							<br/><br/>
							<legend>Creez un compte</legend>
							<br/>
							<?php
							if (isset($_GET['nom']))
							{
								?>
								<span style="color:red;" id="lblLoginIncorrect">
								Cet identifiant existe déjà. Veuillez en saisir un autre.
								</span>
								<?php
							}
							?>
							<br/><br/>
							<table cellpadding="0" cellspacing="0" id="DataTable">
							<tr>
								<td><span id="lblNom">Nom :</span></td>
								<td>
									<input class="champtexte" name="txtNom" type="text" id="txtNom" class="txtBox" required value=<?php echo "'".$NomForm."'"?>/>&nbsp;
								</td>
							</tr>
							<tr>
								<td><span id="lblPrenom">Pr&eacute;nom :</span></td>
								<td>
									<input class="champtexte" name="txtPrenom" type="text" id="txtPrenom" class="txtBox"  required value=<?php echo "'".$PrenomForm."'"?>/>&nbsp;
								</td>
							</tr>
							<tr>
								
								<td><span id="lblLogin">Identifiant :</span></td>
								<td>
									<?php
								if(isset($_GET['nom']))
								{
									?>
									<input style="border-color:red;" class="champtexte" name="txtLogin" type="text" id="txtLogin" class="txtBox" value=""  required />&nbsp;
									<?php
								}
								else
								{
									?>
									<input class="champtexte" name="txtLogin" type="text" id="txtLogin" class="txtBox"  required value=<?php echo "'".$LoginForm."'"?> />&nbsp;
									<?php
								}
								?>
								</td>
							</tr>
							<tr>
								<td><span id="lblMdp">Mot de passe :</span></td>
								<td>
									<input class="champtexte" name="txtMdp" type="password" id="txtMdp" class="mdpBox"  required value=<?php echo "'".$MdpForm."'"?> />&nbsp;
								</td>
							</tr>
							<tr>
								<td><span id="lblAdresse">Adresse :</span></td>
								<td>
									<input class="champtexte" name="txtAdresse" type="text" id="txtAdresse" class="txtBox"  required value=<?php echo "'".$AdresseForm."'"?> />&nbsp;
								</td>
							</tr>
							<tr>
								<td><span id="lblAnneeNaissance">Année de naissance :</span></td>
                                <td>
									<input class="champtexte" name="txtAnneeNaissance" type="text" id="txtAnneeNaissance" class="txtBox"  required value=<?php echo "'".$AnneeNaissanceForm."'"?> />&nbsp;
								</td>

							</tr>
              <tr>
								<td><span id="lblEmail">Adresse email :</span></td>
                                <td>
									<input class="champtexte" name="txtEmail" type="text" id="txtEmail" class="txtBox"  required value=<?php echo "'".$EmailForm."'"?> />&nbsp;
								</td>

							</tr>
							<tr>
							</tr>
							<tr>
								<td colspan=3 id="envoyer">
									<input class="boutonFormulaire" type="reset" name="btAnnuler" value="Annuler"/>
									<input style="margin-right:5px;" class="boutonFormulaire" type="submit" name="btEnvoyer" value="Valider"/>
								</td>
							</tr>
						</table>
						
					</form>
				</td>
			</tr>
		</table>
	</div>
	
	<form action="index.php">
		<button id="retourconnexion">< Retour</button>
	</form>
	
</body>
</html>
