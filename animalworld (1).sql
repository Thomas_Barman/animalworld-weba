-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 30 Janvier 2018 à 18:43
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `animalworld`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateurs`
--

CREATE TABLE `administrateurs` (
  `adm_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (admin)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `administrateurs`
--

INSERT INTO `administrateurs` (`adm_per_id`) VALUES
(75);

-- --------------------------------------------------------

--
-- Structure de la table `animaux`
--

CREATE TABLE `animaux` (
  `anm_id` int(200) NOT NULL,
  `anm_nom` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `animaux`
--

INSERT INTO `animaux` (`anm_id`, `anm_nom`) VALUES
(1, 'chien'),
(2, 'chat'),
(3, 'lapin'),
(4, 'perroquet'),
(5, 'serpent');

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `art_id` int(200) NOT NULL COMMENT 'identifiant de l''article',
  `art_nom` varchar(90) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nom de l''article',
  `art_prix` int(90) NOT NULL COMMENT 'prix de l''article',
  `art_qtestock` int(90) NOT NULL COMMENT 'nombre d''exemplaires de l''article en stock'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`art_id`, `art_nom`, `art_prix`, `art_qtestock`) VALUES
(3, 'os', 10, -5),
(4, 'collier', 10, 25),
(5, 'perchoir', 10, 16),
(6, 'foin', 7, 21),
(7, 'paquet de graines', 8, 17),
(8, 'boite de criquets', 10, 7),
(9, 'cage', 12, 16),
(10, 'laisse', 7, 12),
(11, 'balle de tennis pour chien', 9, 15),
(12, 'souris en peluche', 9, 20);

-- --------------------------------------------------------

--
-- Structure de la table `asso_concerne`
--

CREATE TABLE `asso_concerne` (
  `asc_art_id` int(80) NOT NULL COMMENT 'identifiant de l''article',
  `asc_com_id` int(80) NOT NULL COMMENT 'identifiant de la commande',
  `asc_nbart` int(10) NOT NULL COMMENT 'nombre d''exemplaires d''un article pour une commande'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `asso_concerne`
--

INSERT INTO `asso_concerne` (`asc_art_id`, `asc_com_id`, `asc_nbart`) VALUES
(8, 42, 8);

-- --------------------------------------------------------

--
-- Structure de la table `aw_administrateurs`
--

CREATE TABLE `aw_administrateurs` (
  `adm_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (admin)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_administrateurs`
--

INSERT INTO `aw_administrateurs` (`adm_per_id`) VALUES
(75);

-- --------------------------------------------------------

--
-- Structure de la table `aw_animaux`
--

CREATE TABLE `aw_animaux` (
  `anm_id` int(200) NOT NULL,
  `anm_nom` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_animaux`
--

INSERT INTO `aw_animaux` (`anm_id`, `anm_nom`) VALUES
(1, 'Chien'),
(2, 'Chat'),
(3, 'Lapin'),
(4, 'Perroquet'),
(5, 'Serpent');

-- --------------------------------------------------------

--
-- Structure de la table `aw_articles`
--

CREATE TABLE `aw_articles` (
  `art_id` int(200) NOT NULL COMMENT 'identifiant de l''article',
  `art_nom` varchar(90) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nom de l''article',
  `art_prix` int(90) NOT NULL COMMENT 'prix de l''article',
  `art_image` varchar(99) COLLATE utf8_unicode_ci DEFAULT NULL,
  `art_qtestock` int(90) NOT NULL COMMENT 'nombre d''exemplaires de l''article en stock'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_articles`
--

INSERT INTO `aw_articles` (`art_id`, `art_nom`, `art_prix`, `art_image`, `art_qtestock`) VALUES
(3, 'Os', 10, 'images_articles/os.jpg', -5),
(4, 'Collier', 10, 'images_articles/collier.jpg', 25),
(5, 'Perchoir', 10, 'images_articles/perchoir.jpg', 16),
(6, 'Foin', 7, 'images_articles/foin.jpg', 21),
(7, 'Paquet de graines', 8, 'images_articles/paquet_graines_perroquet.jpg', 17),
(8, 'Boite de criquets', 10, 'images_articles/boite_criquets.jpg', 7),
(9, 'Cage', 12, 'images_articles/cage.jpg', 16),
(10, 'Laisse', 7, 'images_articles/laisse.jpg', 12),
(11, 'Balle pour chien', 9, 'images_articles/balle-tennis-chiens.jpg', 15),
(12, 'Souris en peluche', 9, 'images_articles/souris_peluche.jpg', 20);

-- --------------------------------------------------------

--
-- Structure de la table `aw_asso_concerne`
--

CREATE TABLE `aw_asso_concerne` (
  `asc_art_id` int(80) NOT NULL COMMENT 'identifiant de l''article',
  `asc_com_id` int(80) NOT NULL COMMENT 'identifiant de la commande',
  `asc_nbart` int(10) NOT NULL COMMENT 'nombre d''exemplaires d''un article pour une commande'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_asso_concerne`
--

INSERT INTO `aw_asso_concerne` (`asc_art_id`, `asc_com_id`, `asc_nbart`) VALUES
(8, 42, 8);

-- --------------------------------------------------------

--
-- Structure de la table `aw_clients`
--

CREATE TABLE `aw_clients` (
  `cli_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (client)',
  `cli_datnais` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT 'date de naissance du client',
  `cli_adr` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'adresse du client',
  `cli_email` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email du client'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_clients`
--

INSERT INTO `aw_clients` (`cli_per_id`, `cli_datnais`, `cli_adr`, `cli_email`) VALUES
(77, '1998', '9 passage Anne Yvaud', 'gdl@uranusmail.com'),
(83, '1987', '28 boulevard des eaux-vives', 'jpmartoni@uranusmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `aw_commandes`
--

CREATE TABLE `aw_commandes` (
  `com_id` int(80) NOT NULL COMMENT 'identifiant de la commande',
  `com_cli_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (client)',
  `com_dat` date NOT NULL COMMENT 'date de la commande'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_commandes`
--

INSERT INTO `aw_commandes` (`com_id`, `com_cli_per_id`, `com_dat`) VALUES
(42, 83, '2027-08-17');

-- --------------------------------------------------------

--
-- Structure de la table `aw_commentaires`
--

CREATE TABLE `aw_commentaires` (
  `cmtr_id` int(20) NOT NULL,
  `cmtr_auteur` varchar(70) COLLATE utf8_unicode_ci NOT NULL COMMENT 'auteur du commentaire',
  `cmtr_texte` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cmtr_art_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_commentaires`
--

INSERT INTO `aw_commentaires` (`cmtr_id`, `cmtr_auteur`, `cmtr_texte`, `cmtr_art_id`) VALUES
(1, 'Jean-Paul Martoni', 'C\'est la friandise préférée de mon chien Brutus', 3),
(2, 'Odile Deray', 'Ce collier rouge est très joli. Il coûte pas chez en plus. Je suis très satisfaite de cet article.', 4),
(5, 'Anonyme', 'Ce perchoir est très original car il a une forme d\'arbre', 5),
(6, 'Anonyme', 'Ce perchoir est très utile pour mon perroquet', 5),
(7, 'Anonyme', 'Très utile', 6),
(8, 'Anonyme', 'Mon perroquet les aime bien', 7),
(9, 'Anonyme', 'Mon serpent les aime bien', 8),
(10, 'Anonyme', 'Trop petite pour mon lapin. Il a besoin de plus d\'espace', 9),
(11, 'Anonyme', 'A peine mon chien a commencé à tirer dessus qu\'elle s\'est cassée. J\'avais l\'air malin dehors avec mes deux morceaux de laisse et mon chien que j\'arrivais pas à rappeler.', 10),
(12, 'Anonyme', 'Dès que mon chien à mordu dedans elle s\'est immédiatement dégonflée. Vraiment trop nul cet article', 11),
(13, 'Alphonse', 'Très utile', 12);

-- --------------------------------------------------------

--
-- Structure de la table `aw_correspond`
--

CREATE TABLE `aw_correspond` (
  `cor_art_id` int(200) NOT NULL COMMENT 'identifiant de l''article',
  `cor_anm_id` int(200) NOT NULL COMMENT 'identifiant de l''animal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_correspond`
--

INSERT INTO `aw_correspond` (`cor_art_id`, `cor_anm_id`) VALUES
(3, 1),
(10, 1),
(11, 1),
(4, 2),
(12, 2),
(6, 3),
(9, 3),
(5, 4),
(7, 4),
(8, 5);

-- --------------------------------------------------------

--
-- Structure de la table `aw_personnes`
--

CREATE TABLE `aw_personnes` (
  `per_id` int(230) NOT NULL COMMENT 'identifiant de la personne',
  `per_nom` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nom de la personne',
  `per_pre` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'prenom de la personne',
  `per_login` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'login de la personne',
  `per_mdp` varchar(19844) COLLATE utf8_unicode_ci NOT NULL COMMENT 'mot de passe crypte de la personne'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `aw_personnes`
--

INSERT INTO `aw_personnes` (`per_id`, `per_nom`, `per_pre`, `per_login`, `per_mdp`) VALUES
(75, 'Hono', 'Kim', 'kim', '$2y$10$awbN5WBQH2L0AAL5RN7RoOViC./Bw96H41hOF0CmF3TRtNTP4vcIy'),
(77, 'Daubray-Lacaze', 'Guillaume', 'gdlacase', '$2y$10$lf9jyqYwSU91w5KtzT4pKueBjjbTqSt9jXKlDishbL.EjuqlFMkzu'),
(83, 'Martoni', 'Jean-Paul', 'jpm', '$2y$10$7eUiI1jwLHqXmXiDHEkAKOsD8/LdKgjlVUVOFKz3tPSXWztGJcetG');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `cli_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (client)',
  `cli_datnais` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT 'date de naissance du client',
  `cli_adr` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'adresse du client',
  `cli_email` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email du client'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `clients`
--

INSERT INTO `clients` (`cli_per_id`, `cli_datnais`, `cli_adr`, `cli_email`) VALUES
(77, '1998', '9 passage Anne Yvaud', 'gdl@uranusmail.com'),
(83, '1987', '28 boulevard des eaux-vives', 'jpmartoni@uranusmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE `commandes` (
  `com_id` int(80) NOT NULL COMMENT 'identifiant de la commande',
  `com_cli_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (client)',
  `com_dat` date NOT NULL COMMENT 'date de la commande'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `commandes`
--

INSERT INTO `commandes` (`com_id`, `com_cli_per_id`, `com_dat`) VALUES
(42, 83, '2027-08-17');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `cmtr_id` int(20) NOT NULL,
  `cmtr_texte` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cmtr_art_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `correspond`
--

CREATE TABLE `correspond` (
  `cor_art_id` int(200) NOT NULL COMMENT 'identifiant de l''article',
  `cor_anm_id` int(200) NOT NULL COMMENT 'identifiant de l''animal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `correspond`
--

INSERT INTO `correspond` (`cor_art_id`, `cor_anm_id`) VALUES
(3, 1),
(10, 1),
(11, 1),
(4, 2),
(12, 2),
(6, 3),
(9, 3),
(5, 4),
(7, 4),
(8, 5);

-- --------------------------------------------------------

--
-- Structure de la table `personnes`
--

CREATE TABLE `personnes` (
  `per_id` int(230) NOT NULL COMMENT 'identifiant de la personne',
  `per_nom` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nom de la personne',
  `per_pre` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'prenom de la personne',
  `per_login` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'login de la personne',
  `per_mdp` varchar(19844) COLLATE utf8_unicode_ci NOT NULL COMMENT 'mot de passe crypte de la personne'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `personnes`
--

INSERT INTO `personnes` (`per_id`, `per_nom`, `per_pre`, `per_login`, `per_mdp`) VALUES
(75, 'Hono', 'Kim', 'kim', '$2y$10$awbN5WBQH2L0AAL5RN7RoOViC./Bw96H41hOF0CmF3TRtNTP4vcIy'),
(77, 'Daubray-Lacaze', 'Guillaume', 'gdlacase', '$2y$10$lf9jyqYwSU91w5KtzT4pKueBjjbTqSt9jXKlDishbL.EjuqlFMkzu'),
(83, 'Martoni', 'Jean-Paul', 'jpm', '$2y$10$7eUiI1jwLHqXmXiDHEkAKOsD8/LdKgjlVUVOFKz3tPSXWztGJcetG');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `administrateurs`
--
ALTER TABLE `administrateurs`
  ADD PRIMARY KEY (`adm_per_id`);

--
-- Index pour la table `animaux`
--
ALTER TABLE `animaux`
  ADD PRIMARY KEY (`anm_id`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`art_id`);

--
-- Index pour la table `asso_concerne`
--
ALTER TABLE `asso_concerne`
  ADD PRIMARY KEY (`asc_art_id`,`asc_com_id`),
  ADD KEY `asc_com_id` (`asc_com_id`);

--
-- Index pour la table `aw_administrateurs`
--
ALTER TABLE `aw_administrateurs`
  ADD PRIMARY KEY (`adm_per_id`);

--
-- Index pour la table `aw_animaux`
--
ALTER TABLE `aw_animaux`
  ADD PRIMARY KEY (`anm_id`);

--
-- Index pour la table `aw_articles`
--
ALTER TABLE `aw_articles`
  ADD PRIMARY KEY (`art_id`);

--
-- Index pour la table `aw_asso_concerne`
--
ALTER TABLE `aw_asso_concerne`
  ADD PRIMARY KEY (`asc_art_id`,`asc_com_id`),
  ADD KEY `asc_com_id` (`asc_com_id`);

--
-- Index pour la table `aw_clients`
--
ALTER TABLE `aw_clients`
  ADD PRIMARY KEY (`cli_per_id`);

--
-- Index pour la table `aw_commandes`
--
ALTER TABLE `aw_commandes`
  ADD PRIMARY KEY (`com_id`),
  ADD KEY `com_cli_per_id` (`com_cli_per_id`);

--
-- Index pour la table `aw_commentaires`
--
ALTER TABLE `aw_commentaires`
  ADD PRIMARY KEY (`cmtr_id`),
  ADD KEY `cmtr_art_id` (`cmtr_art_id`);

--
-- Index pour la table `aw_correspond`
--
ALTER TABLE `aw_correspond`
  ADD PRIMARY KEY (`cor_art_id`,`cor_anm_id`),
  ADD KEY `cor_anm_id` (`cor_anm_id`);

--
-- Index pour la table `aw_personnes`
--
ALTER TABLE `aw_personnes`
  ADD PRIMARY KEY (`per_id`),
  ADD UNIQUE KEY `per_login` (`per_login`);

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`cli_per_id`);

--
-- Index pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`com_id`),
  ADD KEY `com_cli_per_id` (`com_cli_per_id`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`cmtr_id`),
  ADD KEY `cmtr_art_id` (`cmtr_art_id`);

--
-- Index pour la table `correspond`
--
ALTER TABLE `correspond`
  ADD PRIMARY KEY (`cor_art_id`,`cor_anm_id`),
  ADD KEY `cor_anm_id` (`cor_anm_id`);

--
-- Index pour la table `personnes`
--
ALTER TABLE `personnes`
  ADD PRIMARY KEY (`per_id`),
  ADD UNIQUE KEY `per_login` (`per_login`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `animaux`
--
ALTER TABLE `animaux`
  MODIFY `anm_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `art_id` int(200) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de l''article', AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `aw_animaux`
--
ALTER TABLE `aw_animaux`
  MODIFY `anm_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `aw_articles`
--
ALTER TABLE `aw_articles`
  MODIFY `art_id` int(200) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de l''article', AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `aw_commandes`
--
ALTER TABLE `aw_commandes`
  MODIFY `com_id` int(80) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de la commande', AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT pour la table `aw_commentaires`
--
ALTER TABLE `aw_commentaires`
  MODIFY `cmtr_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `aw_personnes`
--
ALTER TABLE `aw_personnes`
  MODIFY `per_id` int(230) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de la personne', AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT pour la table `commandes`
--
ALTER TABLE `commandes`
  MODIFY `com_id` int(80) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de la commande', AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `cmtr_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `personnes`
--
ALTER TABLE `personnes`
  MODIFY `per_id` int(230) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de la personne', AUTO_INCREMENT=84;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `administrateurs`
--
ALTER TABLE `administrateurs`
  ADD CONSTRAINT `administrateurs_ibfk_1` FOREIGN KEY (`adm_per_id`) REFERENCES `personnes` (`per_id`);

--
-- Contraintes pour la table `asso_concerne`
--
ALTER TABLE `asso_concerne`
  ADD CONSTRAINT `asso_concerne_ibfk_1` FOREIGN KEY (`asc_com_id`) REFERENCES `commandes` (`com_id`),
  ADD CONSTRAINT `asso_concerne_ibfk_2` FOREIGN KEY (`asc_art_id`) REFERENCES `article` (`art_id`);

--
-- Contraintes pour la table `aw_administrateurs`
--
ALTER TABLE `aw_administrateurs`
  ADD CONSTRAINT `aw_administrateurs_ibfk_1` FOREIGN KEY (`adm_per_id`) REFERENCES `aw_personnes` (`per_id`);

--
-- Contraintes pour la table `aw_asso_concerne`
--
ALTER TABLE `aw_asso_concerne`
  ADD CONSTRAINT `aw_asso_concerne_ibfk_1` FOREIGN KEY (`asc_com_id`) REFERENCES `aw_commandes` (`com_id`),
  ADD CONSTRAINT `aw_asso_concerne_ibfk_2` FOREIGN KEY (`asc_art_id`) REFERENCES `aw_articles` (`art_id`);

--
-- Contraintes pour la table `aw_clients`
--
ALTER TABLE `aw_clients`
  ADD CONSTRAINT `aw_clients_ibfk_1` FOREIGN KEY (`cli_per_id`) REFERENCES `aw_personnes` (`per_id`);

--
-- Contraintes pour la table `aw_commandes`
--
ALTER TABLE `aw_commandes`
  ADD CONSTRAINT `aw_commandes_ibfk_1` FOREIGN KEY (`com_cli_per_id`) REFERENCES `aw_clients` (`cli_per_id`);

--
-- Contraintes pour la table `aw_commentaires`
--
ALTER TABLE `aw_commentaires`
  ADD CONSTRAINT `aw_commentaires_ibfk_1` FOREIGN KEY (`cmtr_art_id`) REFERENCES `aw_articles` (`art_id`);

--
-- Contraintes pour la table `aw_correspond`
--
ALTER TABLE `aw_correspond`
  ADD CONSTRAINT `aw_correspond_ibfk_1` FOREIGN KEY (`cor_art_id`) REFERENCES `aw_articles` (`art_id`),
  ADD CONSTRAINT `aw_correspond_ibfk_2` FOREIGN KEY (`cor_anm_id`) REFERENCES `aw_animaux` (`anm_id`);

--
-- Contraintes pour la table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`cli_per_id`) REFERENCES `personnes` (`per_id`);

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`com_cli_per_id`) REFERENCES `clients` (`cli_per_id`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_ibfk_1` FOREIGN KEY (`cmtr_art_id`) REFERENCES `article` (`art_id`);

--
-- Contraintes pour la table `correspond`
--
ALTER TABLE `correspond`
  ADD CONSTRAINT `correspond_ibfk_1` FOREIGN KEY (`cor_art_id`) REFERENCES `article` (`art_id`),
  ADD CONSTRAINT `correspond_ibfk_2` FOREIGN KEY (`cor_anm_id`) REFERENCES `animaux` (`anm_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
