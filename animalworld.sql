-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 29, 2018 at 02:02 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `animalworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `aw_administrateurs`
--

CREATE TABLE `aw_administrateurs` (
  `adm_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (admin)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_administrateurs`
--

INSERT INTO `aw_administrateurs` (`adm_per_id`) VALUES
(75);

-- --------------------------------------------------------

--
-- Table structure for table `aw_animaux`
--

CREATE TABLE `aw_animaux` (
  `anm_id` int(200) NOT NULL,
  `anm_nom` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_animaux`
--

INSERT INTO `aw_animaux` (`anm_id`, `anm_nom`) VALUES
(1, 'Chien'),
(2, 'Chat'),
(3, 'Lapin'),
(4, 'Perroquet'),
(5, 'Serpent');

-- --------------------------------------------------------

--
-- Table structure for table `aw_articles`
--

CREATE TABLE `aw_articles` (
  `art_id` int(200) NOT NULL COMMENT 'identifiant de l''article',
  `art_nom` varchar(90) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nom de l''article',
  `art_prix` int(90) NOT NULL COMMENT 'prix de l''article',
  `art_image` varchar(99) COLLATE utf8_unicode_ci DEFAULT NULL,
  `art_qtestock` int(90) NOT NULL COMMENT 'nombre d''exemplaires de l''article en stock'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_articles`
--

INSERT INTO `aw_articles` (`art_id`, `art_nom`, `art_prix`, `art_image`, `art_qtestock`) VALUES
(3, 'Os', 10, 'images_articles/os.jpg', -5),
(4, 'Collier', 10, 'images_articles/collier.jpg', 25),
(5, 'Perchoir', 10, 'images_articles/perchoir.jpg', 16),
(6, 'Foin', 7, 'images_articles/foin.jpg', 21),
(7, 'Paquet de graines', 8, 'images_articles/paquet_graines_perroquet.jpg', 17),
(8, 'Boite de criquets', 10, 'images_articles/boite_criquets.jpg', 7),
(9, 'Cage', 12, 'images_articles/cage.jpg', 16),
(10, 'Laisse', 7, 'images_articles/laisse.jpg', 12),
(11, 'Balle pour chien', 9, 'images_articles/balle-tennis-chiens.jpg', 15),
(12, 'Souris en peluche', 9, 'images_articles/souris_peluche.jpg', 20);

-- --------------------------------------------------------

--
-- Table structure for table `aw_asso_concerne`
--

CREATE TABLE `aw_asso_concerne` (
  `asc_art_id` int(80) NOT NULL COMMENT 'identifiant de l''article',
  `asc_com_id` int(80) NOT NULL COMMENT 'identifiant de la commande',
  `asc_nbart` int(10) NOT NULL COMMENT 'nombre d''exemplaires d''un article pour une commande'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_asso_concerne`
--

INSERT INTO `aw_asso_concerne` (`asc_art_id`, `asc_com_id`, `asc_nbart`) VALUES
(8, 42, 8);

-- --------------------------------------------------------

--
-- Table structure for table `aw_clients`
--

CREATE TABLE `aw_clients` (
  `cli_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (client)',
  `cli_datnais` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT 'date de naissance du client',
  `cli_adr` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'adresse du client',
  `cli_email` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email du client'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_clients`
--

INSERT INTO `aw_clients` (`cli_per_id`, `cli_datnais`, `cli_adr`, `cli_email`) VALUES
(77, '1998', '9 passage Anne Yvaud', 'gdl@uranusmail.com'),
(83, '1987', '28 boulevard des eaux-vives', 'jpmartoni@uranusmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `aw_commandes`
--

CREATE TABLE `aw_commandes` (
  `com_id` int(80) NOT NULL COMMENT 'identifiant de la commande',
  `com_cli_per_id` int(230) NOT NULL COMMENT 'identifiant de la personne (client)',
  `com_dat` date NOT NULL COMMENT 'date de la commande'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_commandes`
--

INSERT INTO `aw_commandes` (`com_id`, `com_cli_per_id`, `com_dat`) VALUES
(42, 83, '2027-08-17');

-- --------------------------------------------------------

--
-- Table structure for table `aw_commentaires`
--

CREATE TABLE `aw_commentaires` (
  `cmtr_id` int(20) NOT NULL,
  `cmtr_auteur` varchar(70) COLLATE utf8_unicode_ci NOT NULL COMMENT 'auteur du commentaire',
  `cmtr_texte` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cmtr_art_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_commentaires`
--

INSERT INTO `aw_commentaires` (`cmtr_id`, `cmtr_auteur`, `cmtr_texte`, `cmtr_art_id`) VALUES
(1, 'Jean-Paul Martoni', 'C\'est la friandise préférée de mon chien Brutus', 3),
(2, 'Odile Deray', 'Ce collier rouge est très joli. Il coûte pas chez en plus. Je suis très satisfaite de cet article.', 4),
(5, 'Anonyme', 'Ce perchoir est très original car il a une forme d\'arbre', 5),
(6, 'Anonyme', 'Ce perchoir est très utile pour mon perroquet', 5),
(7, 'Anonyme', 'Très utile', 6),
(8, 'Anonyme', 'Mon perroquet les aime bien', 7),
(9, 'Anonyme', 'Mon serpent les aime bien', 8),
(10, 'Anonyme', 'Trop petite pour mon lapin. Il a besoin de plus d\'espace', 9),
(11, 'Anonyme', 'A peine mon chien a commencé à tirer dessus qu\'elle s\'est cassée. Quelle arnaque! J\'avais l\'air malin dehors avec mes deux morceaux de laisse et mon chien que j\'arrivais pas à rappeler. Je ne suis pas du tout satisfait de cet article', 10),
(12, 'Anonyme', 'Dès que mon chien à mordu dedans elle s\'est immédiatement dégonflée. Vraiment trop nul cet article', 11),
(13, 'Alphonse', 'Très utile', 12);

-- --------------------------------------------------------

--
-- Table structure for table `aw_correspond`
--

CREATE TABLE `aw_correspond` (
  `cor_art_id` int(200) NOT NULL COMMENT 'identifiant de l''article',
  `cor_anm_id` int(200) NOT NULL COMMENT 'identifiant de l''animal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_correspond`
--

INSERT INTO `aw_correspond` (`cor_art_id`, `cor_anm_id`) VALUES
(3, 1),
(10, 1),
(11, 1),
(4, 2),
(12, 2),
(6, 3),
(9, 3),
(5, 4),
(7, 4),
(8, 5);

-- --------------------------------------------------------

--
-- Table structure for table `aw_personnes`
--

CREATE TABLE `aw_personnes` (
  `per_id` int(230) NOT NULL COMMENT 'identifiant de la personne',
  `per_nom` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nom de la personne',
  `per_pre` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'prenom de la personne',
  `per_login` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'login de la personne',
  `per_mdp` varchar(19844) COLLATE utf8_unicode_ci NOT NULL COMMENT 'mot de passe crypte de la personne'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aw_personnes`
--

INSERT INTO `aw_personnes` (`per_id`, `per_nom`, `per_pre`, `per_login`, `per_mdp`) VALUES
(75, 'Hono', 'Kim', 'kim', '$2y$10$awbN5WBQH2L0AAL5RN7RoOViC./Bw96H41hOF0CmF3TRtNTP4vcIy'),
(77, 'Daubray-Lacaze', 'Guillaume', 'gdlacase', '$2y$10$lf9jyqYwSU91w5KtzT4pKueBjjbTqSt9jXKlDishbL.EjuqlFMkzu'),
(83, 'Martoni', 'Jean-Paul', 'jpm', '$2y$10$7eUiI1jwLHqXmXiDHEkAKOsD8/LdKgjlVUVOFKz3tPSXWztGJcetG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aw_administrateurs`
--
ALTER TABLE `aw_administrateurs`
  ADD PRIMARY KEY (`adm_per_id`);

--
-- Indexes for table `aw_animaux`
--
ALTER TABLE `aw_animaux`
  ADD PRIMARY KEY (`anm_id`);

--
-- Indexes for table `aw_articles`
--
ALTER TABLE `aw_articles`
  ADD PRIMARY KEY (`art_id`);

--
-- Indexes for table `aw_asso_concerne`
--
ALTER TABLE `aw_asso_concerne`
  ADD PRIMARY KEY (`asc_art_id`,`asc_com_id`),
  ADD KEY `asc_com_id` (`asc_com_id`);

--
-- Indexes for table `aw_clients`
--
ALTER TABLE `aw_clients`
  ADD PRIMARY KEY (`cli_per_id`);

--
-- Indexes for table `aw_commandes`
--
ALTER TABLE `aw_commandes`
  ADD PRIMARY KEY (`com_id`),
  ADD KEY `com_cli_per_id` (`com_cli_per_id`);

--
-- Indexes for table `aw_commentaires`
--
ALTER TABLE `aw_commentaires`
  ADD PRIMARY KEY (`cmtr_id`),
  ADD KEY `cmtr_art_id` (`cmtr_art_id`);

--
-- Indexes for table `aw_correspond`
--
ALTER TABLE `aw_correspond`
  ADD PRIMARY KEY (`cor_art_id`,`cor_anm_id`),
  ADD KEY `cor_anm_id` (`cor_anm_id`);

--
-- Indexes for table `aw_personnes`
--
ALTER TABLE `aw_personnes`
  ADD PRIMARY KEY (`per_id`),
  ADD UNIQUE KEY `per_login` (`per_login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aw_animaux`
--
ALTER TABLE `aw_animaux`
  MODIFY `anm_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `aw_articles`
--
ALTER TABLE `aw_articles`
  MODIFY `art_id` int(200) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de l''article', AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `aw_commandes`
--
ALTER TABLE `aw_commandes`
  MODIFY `com_id` int(80) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de la commande', AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `aw_commentaires`
--
ALTER TABLE `aw_commentaires`
  MODIFY `cmtr_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `aw_personnes`
--
ALTER TABLE `aw_personnes`
  MODIFY `per_id` int(230) NOT NULL AUTO_INCREMENT COMMENT 'identifiant de la personne', AUTO_INCREMENT=84;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `aw_administrateurs`
--
ALTER TABLE `aw_administrateurs`
  ADD CONSTRAINT `aw_administrateurs_ibfk_1` FOREIGN KEY (`adm_per_id`) REFERENCES `aw_personnes` (`per_id`);

--
-- Constraints for table `aw_asso_concerne`
--
ALTER TABLE `aw_asso_concerne`
  ADD CONSTRAINT `aw_asso_concerne_ibfk_1` FOREIGN KEY (`asc_com_id`) REFERENCES `aw_commandes` (`com_id`),
  ADD CONSTRAINT `aw_asso_concerne_ibfk_2` FOREIGN KEY (`asc_art_id`) REFERENCES `aw_articles` (`art_id`);

--
-- Constraints for table `aw_clients`
--
ALTER TABLE `aw_clients`
  ADD CONSTRAINT `aw_clients_ibfk_1` FOREIGN KEY (`cli_per_id`) REFERENCES `aw_personnes` (`per_id`);

--
-- Constraints for table `aw_commandes`
--
ALTER TABLE `aw_commandes`
  ADD CONSTRAINT `aw_commandes_ibfk_1` FOREIGN KEY (`com_cli_per_id`) REFERENCES `aw_clients` (`cli_per_id`);

--
-- Constraints for table `aw_commentaires`
--
ALTER TABLE `aw_commentaires`
  ADD CONSTRAINT `aw_commentaires_ibfk_1` FOREIGN KEY (`cmtr_art_id`) REFERENCES `aw_articles` (`art_id`);

--
-- Constraints for table `aw_correspond`
--
ALTER TABLE `aw_correspond`
  ADD CONSTRAINT `aw_correspond_ibfk_1` FOREIGN KEY (`cor_art_id`) REFERENCES `aw_articles` (`art_id`),
  ADD CONSTRAINT `aw_correspond_ibfk_2` FOREIGN KEY (`cor_anm_id`) REFERENCES `aw_animaux` (`anm_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
