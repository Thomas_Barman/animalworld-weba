<?php
require_once ("connexion_db.php");
session_start();
if($_SESSION['statut'] != 'mem_connecte')
{
    header('Location: index.php?connecte=false');
}
?>
<!DOCTYPE html>
<!-- Spécifie un document HTML 5 -->
<html>
	<head>
		<!-- En-tête de la page -->

		<meta charset="utf-8" />
		<link rel="stylesheet" href="shop.css">
		<title>Voir les articles</title>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
		
		
		
		
		<script language="javascript">
		
		
		$(document).ready(function(){
			
			
			
			document.getElementById("liste").addEventListener("click", function() {
				
				
				var liste = document.getElementById("liste");
				
				var nomAnimal = liste[liste.selectedIndex].value;

				var XMLHttp = window.XMLHttpRequest ? new XMLHttpRequest(): new ActiveXObject("Microsoft.XMLHTTP");
				console.log("L29");
				if(nomAnimal != "selectionnez")
				{
					
						XMLHttp.onreadystatechange =function()
						{
							//readyState : 
							//0: la requête n'est pas initialisée
							//1: la connexion au serveur est établie
							//2: requête reçue
							//3: requête en cours de traitement
							//4: requête terminée et réponse prète
							console.log("L41. readyState=" + this.readyState);
							if (this.readyState != 4) return;
							if (this.status == 200)
							{	console.log("L44");
								var json = JSON.parse(this.responseText);
								var i;
								var tableau = "<ul style='list-style-type:none;'>";
								
								for(i = 0; i<json.length; i++){
									tableau=tableau
									+"<li>"
										+"<div id='art_com"+json[i].art_id+"' class='article_commentaire' style='text-align:center;'>"
											+"<div style='display:block;flex-direction:column;'>"
											+"<div style='width:205px;height:20px;border-width:2px;border-radius:3px;border-color:green;border-style:solid;margin-left:0px;background-color:#C8FE2E;'>"
													+"<span style='font-size:110%;font-family:arial;float:left;'>"
														+json[i].art_nom
													+"</span>"
													+"<span style='font-size:85%;font-family:arial;font-style:italic;float:right;margin-top:2px;'>"
														+"CHF "
														+json[i].art_prix
														+".- "
													+"</span>"
											+"</div>"
											
											+"<div id='article' class='article'>"
												+"<img style='width:inherit;height:inherit;' src='"+json[i].art_image+"'></img>"
												/*
												+"<br/><br/>"
												+"</p>Aucune image disponible</p>"
												*/
											+"</div>"
											
											+"</div>"
											+"<div id='btnfleche"+json[i].art_id+"' value='"+json[i].art_id+"' class='boutonFleche' onClick='afficherCommentaires(this,"+json[i].art_id+");'>"
												+"<div class='fleche' >></div>"
											+"</div>"
											+"<div id='commentaire"+json[i].art_id+"' class='commentaireHide'>"
											+"</div>"
										+"</div>"
									+"</li>";
								}
								tableau=tableau+"</ul>"
								document.getElementById("article_commentaire").innerHTML = tableau;
								
								
							}
							else
							{
								alert("Erreur"+ this.status);
							}
						};
						console.log("L92");
						XMLHttp.open("GET", "articles.php?nomAnimal=" + nomAnimal, true);
						console.log("L94");
						XMLHttp.send();
				}
				
				
				// Gestion du clic sur les fleches des articles
				afficherCommentaires = function (e, idArticle){
					var XMLHttp = window.XMLHttpRequest ? new XMLHttpRequest(): new ActiveObject("Microsoft.XMLHttp");
					var article = e.getAttribute("value");
					var boutonfleche = e.getAttribute("id");
					
					XMLHttp.onreadystatechange =function()
					{
						
						if (this.readyState != 4) return;
						if (this.status == 200)
						{	
							var json = JSON.parse(this.responseText);
							
							var i;
							var etatAffichageCommentaire = document.getElementById("commentaire"+idArticle).getAttribute("class");
							if(etatAffichageCommentaire == "commentaireHide"){
								document.getElementById(boutonfleche).innerHTML = "<div class='fleche' ><</div>";
							} else{
								document.getElementById(boutonfleche).innerHTML = "<div class='fleche' >></div>";
							}
							
							
							
							var tableau = ""
							+"<div id='comtrs"+idArticle+"' style='display:flex;flex-direction:row;height:220px;width:500px;margin-top:0px;'>"
							for(i = 0; i<json.length; i++)
							{
								
								tableau=tableau
								+"<div style='flex-direction:column;'>"
									+"<div id='"+i+"' style='width:205px;height:20px;border-width:2px;border-radius:3px;border-color:green;border-style:solid;margin-left:0px;background-color:lightgray;margin-left:5px;font-size:110%;font-family:arial;'>"
										+json[i].cmtr_auteur
									+"</div>"
									+"<div id='"+i+"' class='style_commentaire' style='margin-left:5px;overflow:auto;'>"
										+"<br/>"
										+json[i].cmtr_texte
										+"<br/>"
										+"<br/>"
									+"</div>"
								+"</div>"
							
							
							}
							tableau=tableau
								+"<div id='ajouter"+idArticle+"' class='ajoutercommentaire'>"
									+"<form>"
										+"<textarea id='inputPseudonyme"+idArticle+"' placeholder='Votre pseudonyme' style='width:193px; height:16px; border:2px lightgreen solid; border-radius:3px; background-color:lightyellow;'></textarea>"
										+"<textarea id='inputCommentaire"+idArticle+"'placeholder='Ecrivez votre commentaire ici' style='width:193px; height:153px; border:2px lightgreen solid; border-radius:3px; background-color:lightyellow;'></textarea>"
										+"<input id='ajouter' value='Ajouter' type='button' style='width:199px; height:21px; margin-top:2.5px; border:2px lightgreen solid; border-radius:3px; cursor:pointer;' onClick='ajouterCommentaire(this,"+idArticle+");'/>"
									+"</form>"
								+"</div>"
							+"</div>"
							
							
							document.getElementById("commentaire"+article).innerHTML = tableau;
							document.getElementById("commentaire"+article).classList.toggle("commentaireHide");
						}
						else
						{
							alert("Erreur 200"+ this.status);
						}
					};
					
					XMLHttp.open("GET", "commentaires.php?article=" + idArticle, true);

					
					XMLHttp.send();
							
				};
				
				ajouterCommentaire = function (e, idArticle)
				{
					
					
					
					var XMLHttp = window.XMLHttpRequest ? new XMLHttpRequest(): new ActiveObject("Microsoft.XMLHttp");
					var pseudonyme = document.getElementById("inputPseudonyme"+idArticle);
					var commentaire = document.getElementById("inputCommentaire"+idArticle);
					
					XMLHttp.onreadystatechange =function()
					{
						if (this.readyState != 4) return;
						if (this.status == 200)
						{
							if(this.responseText == "ok")
							{
								alert("Votre commentaire est enregistré.");
							}
							else if(this.responseText == "vide")
							{
								alert("Vous ne pouvez pas ajouter un commentaire vide.");
							}
							/*
							var ohe = document.createElement("div"); // Création d'un élément li
							ohe.id
							ohe.class="style_commentaire";
							ohe.textContent = "ohé"; // Définition de son contenu textuel
							
							document.getElementById("comtrs"+idArticle).insertBefore(ohe,
							document.getElementById('ajouter'+idArticle));
							*/
							var etatAffichageCommentaire = document.getElementById("commentaire"+idArticle).getAttribute("class");
							if(etatAffichageCommentaire == "commentaireHide"){
								
								
								
								document.getElementById("btnfleche"+idArticle).innerHTML = "<div class='fleche' ><</div>";
							} else{
								
								document.getElementById("btnfleche"+idArticle).innerHTML = "<div class='fleche' >></div>";
							}
							document.getElementById("commentaire"+idArticle).classList.toggle("commentaireHide");
							
						}
					}
					
					XMLHttp.open("GET", "ajouter_commentaire.php?idArticle="+idArticle+"&commentaire_text="+commentaire.value+"&commentaire_auteur="+pseudonyme.value, true);
					XMLHttp.send();
				}
			});
		});
		
			
		</script>

	</head>

	<body>
		
		<div id="mozaique"></div><!--Cette div contient le "ruban" blanc avec les animaux-->
		
		<div id="rectangle">
			<div id="voicinosarticles">
				<div>Voici nos articles</div>
				
				<select id="liste" name="listeDeroulante"><!--Liste déroulante avec des animaux-->
					<option id="titreListe" class="option" value="selectionnez" selected>selectionnez</option>
					<option id="titreListe" class="option" value="Tout">Tout</option>
					<?php
						try
						{
							$reponse = $bdd->query("SELECT * FROM aw_animaux");
							while ($donnees = $reponse->fetch())
							{
							?>
								<option class="option" value="<?php echo $donnees['anm_nom']?>"><?php echo $donnees['anm_nom']?></option>
							<?php
							}
							$bdd = null;
						}
						catch (PDOException $e)
						{
        		echo "Erreur !: " . $e->getMessage() . "<br />";
        		die();
						}
					?>


				</select>
				
				
			</div>
		</div>
		<br/><br/>
			<div id="article_commentaire"><!--class="article_commentaire"-->
			
			</div>
			
		
		<script language="javascript">
			/*titreAffiche = true;

			document.getElementById("liste").addEventListener("mousedown", supprimerTitreListe);

			function supprimerTitreListe()
			{
				if (titreAffiche == true)
				{
					document.getElementById("liste").removeChild(document.getElementById("titreListe"));
				}
			}*/
		</script>
	</body>
	<!--sources: http://lutibetha.alwaysdata.net/index.php/inscription.php https://www.w3schools.com/xml/ajax_xmlhttprequest_create.asp -->
</html>
