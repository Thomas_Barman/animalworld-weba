<?php
require_once ("connexion_db.php");

$article = $_GET["article"];

try {

	//fetchAll prend tout et evite les doublons
  $reponse = $bdd->query("select aw_commentaires.cmtr_auteur, aw_commentaires.cmtr_texte 
  from aw_commentaires 
  join aw_articles on aw_commentaires.cmtr_art_id = aw_articles.art_id 
  where aw_commentaires.cmtr_art_id = '$article'") or die(print_r($bdd->errorInfo()));

	$desCommentaires = $reponse->fetchAll(PDO::FETCH_ASSOC);

	$bdd = null;

	echo json_encode($desCommentaires);

}
catch (PDOException $e) {
   echo "Erreur !: " . $e->getMessage() . "<br />";
   die();
}

?>